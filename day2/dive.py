example = ['forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2']

movements = {'forward': [1, 0], 'down': [0, 1], 'up': [0, -1]}

# Part 1
# example
# pos = 0
# depth = 0
# for item in example:
#     mov, amount = item.split(' ')
#
#     pos += movements[mov][0] * int(amount)
#     depth += movements[mov][1] * int(amount)
#
# print('example res')
# print(pos * depth)

pos = 0
depth = 0
with open('pilot') as f:
    for line in f:
        mov, amount = line.split(' ')

        pos += movements[mov][0] * int(amount)
        depth += movements[mov][1] * int(amount)

print('res part 1')
print(pos * depth)

# Part 2
# pos = 0
# depth = 0
# aim = 0
# for item in example:
#     mov, amount = item.split(' ')
#     if mov == 'down':
#         aim += int(amount)
#     if mov == 'up':
#         aim += -int(amount)
#     if mov == 'forward':
#         pos += int(amount)
#         depth += aim * int(amount)

pos = 0
depth = 0
aim = 0
with open('pilot') as f:
    for line in f:
        mov, amount = line.split(' ')
        if mov == 'down':
            aim += int(amount)
        if mov == 'up':
            aim += -int(amount)
        if mov == 'forward':
            pos += int(amount)
            depth += aim * int(amount)

print('res part 2')
print(pos * depth)
