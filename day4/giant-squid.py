import numpy as np

with open('bingo') as f:
    raw_data = f.read().splitlines()

print(raw_data[0])
draws = [int(e) for e in raw_data[0].split(',') if e != '']

# setup board
boards = [np.empty((5, 5)).astype(int)]
board_id = 0
row_id = 0
for line in raw_data[2:]:
    if line == '':
        boards.append(np.empty((5, 5)).astype(int))
        board_id += 1
        row_id = 0
        continue

    row = [int(e) for e in line.split(' ') if e != '']
    boards[board_id][row_id, :] = row
    row_id += 1

boards_state = []
for board in boards:
    boards_state.append(np.full((5, 5), False))
    print('')
    print(board)

# check draws
scores = [0 for board in boards]
when = [0 for board in boards]

for (i, draw) in enumerate(draws):

    for board_id in range(len(boards)):
        match = boards[board_id] == draw
        boards_state[board_id] = boards_state[board_id] + match

    for board_id in range(len(boards)):
        if scores[board_id] != 0:
            continue
        for row in range(5):
            if all(boards_state[board_id][row, :]):
                unmarked = boards_state[board_id] != True
                scores[board_id] = sum(boards[board_id][unmarked]) * draw
                when[board_id] = i
                break

        for col in range(5):
            if all(boards_state[board_id][:, col]):
                unmarked = boards_state[board_id] != True
                scores[board_id] = sum(boards[board_id][unmarked]) * draw
                when[board_id] = i
                break

print(scores)
print(when)

print(scores[when.index(max(when))])


