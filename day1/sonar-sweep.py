import numpy as np

example = np.array([199, 200, 208, 210, 200, 207, 240, 269, 260, 263])

with open('sensor-data', 'r') as f:
    data = np.loadtxt(f)

# part 1
print(sum(np.diff(data) > 0))

# part 2
moving_avg = (data[0:-2] + data[1:-1] + data[2:]) / 3
print(sum(np.diff(moving_avg) > 0))
