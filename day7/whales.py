with open('data') as f:
    positions = [int(e) for e in f.readline().split(',')]

search = True
x = round(sum(positions) / len(positions))


def fuel(positions, x):
    # return sum([abs(pos - x) for pos in positions])  ## part 1
    return sum([sum(range(abs(pos - x)+1)) for pos in positions])


while search:
    base_fuel = fuel(positions, x)
    up_fuel = fuel(positions, x+1)
    down_fuel = fuel(positions, x-1)

    if base_fuel < min(up_fuel, down_fuel):
        search = False

    if up_fuel < base_fuel:
        x = x+1

    if down_fuel < base_fuel:
        x = x-1

print(fuel(positions, x), x)