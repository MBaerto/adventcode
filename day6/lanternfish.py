with open('data') as f:
    fish_raw = [int(e) for e in f.readline().split(',')]

N = 256
fish = []
for i in range(9):
    fish.append(len([fi for fi in fish_raw if fi == i]))

for day in range(N):
    amount_born = fish[0]
    fish = fish[1:] + [amount_born]
    fish[6] += amount_born

print(sum(fish))