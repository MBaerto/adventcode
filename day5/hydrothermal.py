import numpy as np

with open('data') as f:
    raw_data = f.read().splitlines()

maxvals = {'x': 0, 'y': 0}
clean_data = []
for item in raw_data:
    start, stop = [[int(v) for v in e.split(',')] for e in item.replace(' ','').split('->')]

    maxvals['x'] = max(maxvals['x'], max(start[0], stop[0]))
    maxvals['y'] = max(maxvals['y'], max(start[1], stop[1]))
    clean_data.append([start, stop])

grid = np.full((maxvals['x']+1, maxvals['y']+1), 0)
for item in clean_data:
    print('')
    print(item)
    x_range = [e[0] for e in item]
    y_range = [e[1] for e in item]

    if x_range[0] == x_range[1]:
        for y_val in range(min(y_range), max(y_range) + 1):
            grid[x_range[0], y_val] += 1

    elif y_range[0] == y_range[1]:
        for x_val in range(min(x_range), max(x_range) + 1):
            grid[x_val, y_range[0]] += 1

    else:
        x_sign = np.sign(x_range[1] - x_range[0])
        y_sign = np.sign(y_range[1] - y_range[0])
        x_vals = list(range(x_range[0], x_range[1] + x_sign, x_sign))
        y_vals = list(range(y_range[0], y_range[1] + y_sign, y_sign))
        print(x_vals)
        print(y_vals)
        for i in range(len(x_vals)):
            grid[x_vals[i], y_vals[i]] += 1

print(grid.T)
print(sum(grid.flatten() > 1))