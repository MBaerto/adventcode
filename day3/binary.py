import numpy as np

# part 1
bin_data = []
bin_data_raw = []
with open('bin-input') as f:
    for line in f:
        cl_line = line.replace('\n', '')
        bin_data_raw.append(cl_line)
        if len(bin_data) == 0:
            bin_data = [[int(e)] for e in cl_line]
            continue
        for i in range(len(cl_line)):
            bin_data[i].append(int(cl_line[i]))

gamma_bin = np.median(bin_data, axis=1)
gamma_val = sum([e*2**i for (i, e) in enumerate(gamma_bin[::-1])])
print(gamma_val)

epsilon_bin = gamma_bin == 0
epsilon_val = sum([e*2**i for (i, e) in enumerate(epsilon_bin[::-1])])
print(epsilon_val)

print(gamma_val * epsilon_val)

# part 2
bin_data_np = np.array(bin_data)
ox_indices = [True for i in range(len(bin_data_np[0]))]
co2_indices = [True for i in range(len(bin_data_np[0]))]
for i in range(len(bin_data_np)):
    most_common = np.ceil(np.median(bin_data_np[i][ox_indices]))
    new_ox_indices = bin_data_np[i] == most_common
    ox_indices = [i and j for (i, j) in zip(ox_indices, new_ox_indices)]
    if sum(ox_indices) == 1:
        break

for i in range(len(bin_data_np)):
    most_common = np.ceil(np.median(bin_data_np[i][co2_indices]))
    new_co2_indices = bin_data_np[i] != most_common
    co2_indices = [i and j for (i, j) in zip(co2_indices, new_co2_indices)]
    if sum(co2_indices) == 1:
        break


ox_bin_value = np.array([e[ox_indices] for e in bin_data_np]).flatten()
ox_val = sum([e*2**i for (i, e) in enumerate(ox_bin_value[::-1])])
print(ox_val)

co2_bin_value = np.array([e[co2_indices] for e in bin_data_np]).flatten()
co2_val = sum([e*2**i for (i, e) in enumerate(co2_bin_value[::-1])])
print(co2_val)

print(ox_val * co2_val)
